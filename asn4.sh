#!/bin/bash

 if [ "$#" -eq  "0" ]
   then
     echo "No arguments supplied"
     echo "Usage: ./asn_v4.sh <ASN> "
     exit
else
curl --silent https://www.dan.me.uk/bgplookup?asn=$1 |grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\/[0-9]\{1,2\}' |sort |uniq

 fi

# This lil script will extract all the advertised BGP ipv4 prefixes from asn #
# Example Run:  (Extracting prefix adv. from asn 5601)
# ./asn4.sh 5601
# 192.36.236.0/24
# 192.36.253.0/24
# 194.132.192.0/22

