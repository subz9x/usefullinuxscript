Collection of very useful Linux scripts (Mainly for Ubuntu/Kali) to do various tasks that would normally take a while to script:

extract_ip_auth.sh - read /var/log/auth.log - extract all IPs of failed attempted logins - add to IPTABLES drop
asn4.sh - Extract all prefixs (subnets) ipv4 advertised out ASN #
wrt_blk.sh - Extracts sub/domains from a /22 (this is the biggest range recommended) scanning rtsak for the range 74.224.10.0/22


Courtesy of dj substance
https://tranceattic.com
https://9x.network

Come join us #9x / EFNet

