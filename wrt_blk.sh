#/bin/bash
echo "scanning rtsak for the range $1.0/22";echo
lynx --dump "https://www.rtsak.com/cidr/$1.0-22"| grep dns| cut -d "/" -f 5
echo "done";echo

# Script that will query site rtsak.com to extract/enumerate domain/subdomains from a /22 
# Example: ./wrt_blk.sh 74.224.10
################################################
# scanning rtsak for the range 74.224.10.0/22
#
# adsl-74-224-8-41.asm.bellsouth.net
# adsl-224-8-42.asm.bellsouth.net
# adsl-74-224-9-2.asm.bellsouth.net
# adsl-224-10-115.asm.bellsouth.net
# adsl-74-224-11-2.asm.bellsouth.net
# adsl-224-11-219.asm.bellsouth.net

